﻿using System;

namespace week3_ex14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What month were you born in?");
            var birthmonth = (Console.ReadLine());

            Console.WriteLine($"You were born in {birthmonth}");
        }
    }
}
